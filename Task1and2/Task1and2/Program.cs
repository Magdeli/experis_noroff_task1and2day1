﻿using System;

namespace Task1and2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Dean and Greg, hope you have a nice day! My name is Magdeli.");

            Console.WriteLine("Please type in your name:");
            string name = Console.ReadLine();
            Console.WriteLine("Hello " + name + ", your name is " + name.Length + " characters long and starts with a " + name[0] + ".");
        }
    }
}
